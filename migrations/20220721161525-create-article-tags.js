"use strict";
module.exports = {
	async up(queryInterface, Sequelize) {
		await queryInterface.createTable("article_tags", {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER,
			},
			article_id: {
				type: Sequelize.INTEGER,
			},
			tag_id: {
				type: Sequelize.INTEGER,
			},
			createdAt: {
				allowNull: false,
				type: Sequelize.DATE,
			},
			updatedAt: {
				allowNull: false,
				type: Sequelize.DATE,
			},
		});
	},
	async down(queryInterface, Sequelize) {
		await queryInterface.dropTable("article_tags");
	},
};
