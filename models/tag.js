"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class Tag extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate({ Article }) {
			// define association here
			Tag.belongsToMany(Article, { through: "articleTags" });
		}
	}
	Tag.init(
		{
			id: {
				type: DataTypes.INTEGER,
				primaryKey: true,
				autoIncrement: true,
			},
			tag_name: DataTypes.STRING,
		},
		{
			sequelize,
			modelName: "Tag",
			tableName: "tags",
		}
	);
	return Tag;
};
