"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class articleTags extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate({ article, Tag }) {
			// define association here
			// articleTags.hasMany(article, {
			// 	foreignKey: "id",
			// });
			// articleTags.belongsToMany(article, {
			// 	through: "article_id",
			// });
			// articleTags.belongsToMany(Tag, {
			// 	foreignKey: "tag_id",
			// });

			// tidak perlu kasih apapun karena ibaratnya cuma lewat
		}
	}
	articleTags.init(
		{
			id: {
				type: DataTypes.INTEGER,
				primaryKey: true,
				autoIncrement: true,
			},
			article_id: DataTypes.INTEGER,
			tag_id: DataTypes.INTEGER,
		},
		{
			sequelize,
			modelName: "articleTags",
			tableName: "article_tags",
		}
	);
	return articleTags;
};
